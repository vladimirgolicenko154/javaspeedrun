package lesson4;

public class Animal {

    private String name;

    private int age;

    private boolean isHungry;

    private AnimalType animalType;

    public Animal() {

    }

    public Animal(String name, int age, boolean isHungry, AnimalType animalType) {
        this.name = name;
        this.age = age;
        this.isHungry = isHungry;
        this.animalType = animalType;
    }

    public void printInfo() {
        System.out.println("My names " + name + ", I'm " + animalType + ". My age is " + age + ". " + isHungry() + " now");
    }

    private String isHungry() {
        return isHungry ? "I'm hungry" : "I'm not hungry";
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHungry(boolean hungry) {
        isHungry = hungry;
    }

}
