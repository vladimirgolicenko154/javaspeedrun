package lesson4;

import java.util.Arrays;
import java.util.List;

public class ZooAnimalsChanger {
    public static void main(String[] args) {

        Zoo odessaZoo = new Zoo("Odessa zoo", "Odessa",
                Arrays.asList(new Animal("Buffy", 3, true, AnimalType.FOX),
                        new Animal("Jack", 5, false, AnimalType.ELEPHANT),
                        new Animal("Maya", 7, false, AnimalType.MONKEY),
                        new Animal("Lora", 1, true, AnimalType.LION)));


        Zoo nikolaevZoo = new Zoo("Nikolaev zoo", "Nikolaev",
                Arrays.asList(new Animal("Tim", 9, false, AnimalType.CAMEL),
                        new Animal("Jora", 10, true, AnimalType.CROCODILE)));


        // before change
        odessaZoo.printInfo();
        odessaZoo.printAllAnimals();

        System.out.println();
        nikolaevZoo.printInfo();
        nikolaevZoo.printAllAnimals();

        // change
        changeAnimals(odessaZoo, nikolaevZoo);

        // after change
        System.out.println("\n------AFTER------\n");
        odessaZoo.printInfo();
        odessaZoo.printAllAnimals();

        System.out.println();
        nikolaevZoo.printInfo();
        nikolaevZoo.printAllAnimals();

        // change  objects state
        nikolaevZoo.setName("Biopark Nikolaev");
        odessaZoo.getAnimals().get(1).setHungry(false);
        odessaZoo.getAnimals().get(0).setAge(11);


        System.out.println("\n-------After objects state changed-------\n");
        nikolaevZoo.printInfo();
        odessaZoo.printAllAnimals();


    }

    private static void changeAnimals(Zoo zoo1, Zoo zoo2) {
        List<Animal> temp = zoo2.getAnimals();
        zoo2.setAnimals(zoo1.getAnimals());
        zoo1.setAnimals(temp);
    }
}
