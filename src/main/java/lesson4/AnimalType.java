package lesson4;

public enum AnimalType {
    LION, CROCODILE, ELEPHANT, BEAR, CAMEL, DONKEY, EAGLE, FOX, MONKEY, GIRAFFE;
}
