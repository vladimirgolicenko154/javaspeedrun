package lesson4;

import java.util.List;

public class Zoo {

    private String name;

    private String city;

    private List<Animal> animals;

    public Zoo() {

    }

    public Zoo(String name, String city, List<Animal> animals) {
        this.name = name;
        this.city = city;
        this.animals = animals;
    }

    public void printInfo() {
        System.out.println("Zoo name: " + name + ". City: " + city + ". Animals number: " + animals.size());
    }

    public void printAllAnimals() {
        animals.forEach(Animal::printInfo);
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setAnimals(List<Animal> animals) {
        this.animals = animals;
    }

    public List<Animal> getAnimals() {
        return animals;
    }
}
