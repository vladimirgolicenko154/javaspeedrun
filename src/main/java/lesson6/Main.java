package lesson6;

import lesson6.academy.Academy;
import lesson6.heroes.Hero;
import lesson6.heroes.healer.Healer;
import lesson6.heroes.healer.HealerDruid;
import lesson6.heroes.healer.beast.Beast;
import lesson6.heroes.warrior.Cossack;
import lesson6.heroes.warrior.Warrior;
import lesson6.weapons.Saber;

public class Main {
    public static void main(String[] args) {
        Warrior cossack = new Cossack(100, 3, 30, 1, new Saber("saber", 35));
        Healer healerDruid = new HealerDruid(100, 7, 2, 20, new Beast(100, 50, "beast"));
        Academy academy = new Academy();

        //before
        showDamage(cossack);
        showDamage(healerDruid);

        academy.teach((Trainee) cossack);
        academy.teach((Trainee) healerDruid);


    }

    private static void showDamage(Hero hero) {
        System.out.println(hero.getClass().getSimpleName() + " : " + hero.getDamage());
    }
}
