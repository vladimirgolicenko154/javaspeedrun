package lesson6.academy;

import lesson6.Trainee;
import lesson6.heroes.warrior.Warrior;

public class WarriorAcademy extends Academy {

    @Override
    public void teach(Trainee trainee) {
        if (trainee instanceof Warrior)
            super.teach(trainee);
    }
}
