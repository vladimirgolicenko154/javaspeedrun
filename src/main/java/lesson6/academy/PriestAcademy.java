package lesson6.academy;

import lesson6.Trainee;
import lesson6.heroes.healer.Healer;

public class PriestAcademy extends Academy {
    @Override
    public void teach(Trainee trainee) {
        if (trainee instanceof Healer)
            super.teach(trainee);
    }
}
