package lesson6.academy;

import lesson6.Trainee;

public class Academy {

    public void teach(Trainee trainee) {
        trainee.train();
    }
}
