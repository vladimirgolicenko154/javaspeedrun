package lesson6.weapons;

import lesson6.Informational;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class Sword implements Informational {

    protected String name;

    protected int damage;

    public Sword(String name, int damage) {
        this.name = name;
        this.damage = damage;
    }

    @Override
    public void info() {
        System.out.print("name=" + name + ", damage=" + damage);
    }
}
