package lesson6.weapons;

import lesson6.Informational;

public final class Saber extends Sword implements Informational {
    public Saber(String name, int damage) {
        super(name, damage);
    }

    @Override
    public void info() {
        super.info();
    }
}
