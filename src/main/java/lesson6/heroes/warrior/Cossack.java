package lesson6.heroes.warrior;

import lesson6.Informational;
import lesson6.Trainee;
import lesson6.heroes.Hero;
import lesson6.weapons.Saber;

public class Cossack extends Warrior implements Trainee, Informational {

    private Saber saber;

    public Cossack(int hp, int level, int damage, int damageMultiplier, Saber saber) {
        super(hp, level, damage, damageMultiplier);
        this.saber = saber;
    }

    public int saber(Hero hero) {
        if (this == hero)
            return 0;
        int hpAfterDamage = hero.getHp() - (saber.getDamage());
        hero.setHp(hpAfterDamage);
        if (hpAfterDamage <= 0) {
            System.out.println(hero.getClass().getSimpleName() + " died");
            return -1;
        }
        return hpAfterDamage;
    }

    @Override
    public void info() {
        super.info();
        System.out.println(", saber=" + saber);
    }

    @Override
    public void train() {
        this.damage *= 2;
        System.out.println("Damage is raised up to " + this.damage);
    }
}
