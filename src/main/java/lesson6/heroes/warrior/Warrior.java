package lesson6.heroes.warrior;

import lesson6.Informational;
import lesson6.heroes.Hero;

public abstract class Warrior extends Hero implements Informational {

    private int damageMultiplier;

    public Warrior(int hp, int level, int damage, int damageMultiplier) {
        super(hp, level, damage);
        this.damageMultiplier = damageMultiplier;
    }

    public int strongHit(Hero hero) {
        if (this == hero)
            return 0;
        int hpAfterDamage = hero.getHp() - (this.damage * damageMultiplier);
        hero.setHp(hpAfterDamage);
        if (hpAfterDamage <= 0) {
            System.out.println(hero.getClass().getSimpleName() + " died");
            return -1;
        }
        return hpAfterDamage;
    }

    @Override
    public void info() {
        super.info();
        System.out.print(", damageMultiplier=" + damageMultiplier);
    }
}
