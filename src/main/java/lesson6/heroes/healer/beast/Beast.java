package lesson6.heroes.healer.beast;


import lesson6.Informational;
import lesson6.heroes.Hero;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class Beast implements Informational {
    private int hp;

    private int power;

    private String name;

    public final int beastAttack(Hero hero) {
        int hpAfterDamage = hero.getHp() - this.power;
        hero.setHp(hpAfterDamage);
        if (hero.getHp() <= 0) {
            System.out.println(hero.getClass().getSimpleName() + " died!");
            return -1;
        }
        return hpAfterDamage;
    }

    @Override
    public void info() {
        System.out.print("hp=" + hp + ", power=" + power + ", name=" + name);
    }
}
