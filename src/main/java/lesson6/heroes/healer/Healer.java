package lesson6.heroes.healer;

import lesson6.Informational;
import lesson6.heroes.Hero;

public abstract class Healer extends Hero implements Informational {

    private int healingPower;

    public Healer(int hp, int level, int damage, int healingPower) {
        super(hp, level, damage);
        this.healingPower = healingPower;
    }

    public int treat(Hero hero) {
        if (this == hero)
            return 0;
        int treatmentHp = hero.getHp() + healingPower;
        hero.setHp(treatmentHp);
        if (hero.getHp() > 100)
            hero.setHp(100);
        return treatmentHp;
    }

    public int treat() {
        this.hp += healingPower;
        if (hp > 100)
            hp = 100;
        return hp;
    }

    @Override
    public void info() {
        super.info();
        System.out.print(", healingPower=" + healingPower);
    }
}
