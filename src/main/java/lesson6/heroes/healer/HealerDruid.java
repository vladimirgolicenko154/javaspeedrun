package lesson6.heroes.healer;

import lesson6.Informational;
import lesson6.Trainee;
import lesson6.heroes.healer.beast.Beast;
import lombok.Getter;


public class HealerDruid extends Healer implements Trainee, Informational {

    @Getter
    private Beast beast;

    public HealerDruid(int hp, int level, int damage, int healingPower) {
        super(hp, level, damage, healingPower);
    }

    public HealerDruid(int hp, int level, int damage, int healingPower, Beast beast) {
        super(hp, level, damage, healingPower);
        this.beast = beast;
    }

    public boolean tameBeast(Beast beast) {
        if (this.beast != null) {
            System.out.println("You already have the beast!");
            return false;
        }
        this.beast = beast;
        return true;
    }

    @Override
    public void info() {
        super.info();
        System.out.println(", beast=" + beast);
    }

    @Override
    public void train() {
        this.damage *= 2;
        System.out.println("Damage is raised up to " + this.damage);
    }
}
