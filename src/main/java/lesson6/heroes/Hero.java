package lesson6.heroes;

import lesson6.Informational;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public abstract class Hero implements Informational {

    protected int hp;

    protected int level;

    protected int damage;

    public void info() {
        System.out.print("hp=" + hp + ", level=" + level + ", damage=" + damage);
    }
}
