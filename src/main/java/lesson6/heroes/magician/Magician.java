package lesson6.heroes.magician;

import lesson6.Informational;
import lesson6.heroes.Hero;

public abstract class Magician extends Hero implements Informational {
    protected int magicDamage;

    public Magician(int hp, int level, int damage, int magicDamage) {
        super(hp, level, damage);
        this.magicDamage = magicDamage;
    }

    public void magicAttack(Hero hero) {
        hero.setHp(hero.getHp() - magicDamage);
    }

    @Override
    public void info() {
        super.info();
        System.out.println(", magicDamage=" + magicDamage);
    }
}
