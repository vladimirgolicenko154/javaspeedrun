package lesson6.heroes.magician;

import lesson6.Informational;
import lesson6.Trainee;

public class DarkMagician extends Magician implements Trainee, Informational {

    public DarkMagician(int hp, int level, int damage, int magicDamage) {
        super(hp, level, damage, magicDamage);
    }

    @Override
    public void train() {
        this.magicDamage *= 2;
        System.out.println("Damage is raised up to " + this.magicDamage);
    }
}
