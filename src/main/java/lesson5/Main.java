package lesson5;

import lesson5.account.BankAccountManager;
import lesson5.model.Person;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Person vova = new Person("Vova", "Holi", "22-33", "vova@gmail.com", LocalDate.now());
        Person oleg = new Person("Oleg", "Pup", "55-22", "oleg@gmail.com", LocalDate.now().minusDays(3));
        Person arsen = new Person("Arsen", "Bim", "99-44", "arsen@gmail.com", LocalDate.now().minusDays(5));

        // create BankAccountManager
        BankAccountManager bankAccountManager = new BankAccountManager();

        // add bank accounts
        bankAccountManager.add(vova, 500);
        bankAccountManager.add(oleg, 700);
        bankAccountManager.add(arsen, 950);

        String id = bankAccountManager.getAccounts()[0].getId();
        String falseId = "some id";

        // call method findBankAccountById passing in a non-existent and existing id
        System.out.println(bankAccountManager.findBankAccountById(falseId)); // null
        System.out.println(bankAccountManager.findBankAccountById(id)); // BankAccount object

        // call method deleteBankAccountById passing in a non-existent and existing id
        System.out.println(bankAccountManager.deleteBankAccountById(falseId)); // false
        System.out.println(bankAccountManager.deleteBankAccountById(id)); // true
        // after deletion
        System.out.println(bankAccountManager.findBankAccountById(id)); // null


        String idToChangeBalance = bankAccountManager.getAccounts()[1].getId();
        System.out.println(bankAccountManager.getAccounts()[1].getBalance()); // before
        // top up account
        bankAccountManager.topUpAccount(idToChangeBalance, 500);
        System.out.println(bankAccountManager.getAccounts()[1].getBalance()); // after


        String first = bankAccountManager.getAccounts()[0].getId();
        String second = bankAccountManager.getAccounts()[1].getId();

        // transfer money between accounts
        bankAccountManager.transfer(first, second, 300);
        bankAccountManager.transfer(second, first, 500);
        bankAccountManager.transfer(second, first, 300);
        bankAccountManager.transfer(first, second, 1000);

        // output info about accounts and their transaction history
        System.out.println(bankAccountManager.findBankAccountById(first));
        System.out.println(bankAccountManager.findBankAccountById(second));

    }
}
