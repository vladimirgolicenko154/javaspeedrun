package lesson5.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public
class Person {
    private String name;
    private String surname;
    private String phone;
    private String email;
    private LocalDate birthDate;
}
