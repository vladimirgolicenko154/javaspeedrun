package lesson5.account;

import lesson5.model.Person;
import lombok.Getter;

import java.util.Arrays;

import static java.time.LocalDateTime.now;

public class BankAccountManager {

    /**
     * to get {@link BankAccount#id} in Main as id is unique and unpredictable
     */
    @Getter
    private BankAccount[] accounts;

    /**
     * for the logic of increasing the size of the {@link #accounts}
     */
    private int currentSize = 0;

    public BankAccountManager() {
        accounts = new BankAccount[10];
    }

    public BankAccount add(Person owner, double balance) {
        if (currentSize == accounts.length) {
            this.accounts = newList(accounts);
        }
        return accounts[currentSize++] = new BankAccount(balance, owner);
    }

    public BankAccount findBankAccountById(String id) {
        return Arrays.stream(accounts)
                .filter(bankAccount -> bankAccount != null && bankAccount.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public boolean deleteBankAccountById(String id) {
        if (id == null || id.isBlank() || findBankAccountById(id) == null)
            return false;

        for (int i = 0; i < currentSize; i++) {
            if (accounts[i].getId().equals(id)) {
                deleteAccount(i);
                return true;
            }
        }
        return false;
    }

    public void topUpAccount(String accountId, double amount) {
        if (amount <= 0) {
            System.out.println("Incorrect amount: " + amount);
            return;
        }

        BankAccount bankAccountById = findBankAccountById(accountId);
        if (bankAccountById == null) {
            System.out.println("No account with id: " + accountId);
            return;
        }

        bankAccountById.setBalance(bankAccountById.getBalance() + amount);
        bankAccountById.addNewTransaction("Income: +" + amount + "\t|\t FROM: ATM" +
                "" + "\t|\tTime: " + now());
    }

    public void transfer(String fromAccountId, String toAccountId, double amount) {
        BankAccount from = findBankAccountById(fromAccountId);
        BankAccount to = findBankAccountById(toAccountId);
        if (from == null || to == null) {
            System.out.println("No such account!");
            return;
        }

        if (amount <= 0) {
            System.out.println("Incorrect amount: " + amount);
            return;
        }

        if (from.getBalance() < amount) {
            System.out.println("Not enough funds to write off!");
            from.addNewTransaction("Unsuccessful attempt to transfer funds: insufficient funds.\t|\t Time: " + now());
            return;
        }

        from.setBalance(from.getBalance() - amount);
        from.addNewTransaction("Expense: -" + amount + "\t|\t TO:"
                + to.getOwner().getName() + " " + to.getOwner().getSurname() + "\t|\tTime: " + now());

        to.setBalance(to.getBalance() + amount);
        to.addNewTransaction("Income: +" + amount + "\t|\t FROM:"
                + from.getOwner().getName() + " " + from.getOwner().getSurname() + "\t|\tTime: " + now());
    }

    private void deleteAccount(int index) {
        accounts[index] = null;
        if (index == currentSize - 1) {
            currentSize--;
            return;
        }

        for (int i = index; i < currentSize; i++) {
            if (i == currentSize - 1) {
                accounts[i] = null;
                break;
            }
            accounts[i] = accounts[i + 1];
        }
        currentSize--;
    }

    private BankAccount[] newList(BankAccount[] accounts) {
        BankAccount[] newAccounts = new BankAccount[accounts.length * 3 / 2 + 1];
        int i = 0;
        for (BankAccount s : accounts) {
            newAccounts[i] = s;
            i++;
        }
        return newAccounts;
    }
}
