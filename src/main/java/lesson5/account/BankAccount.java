package lesson5.account;

import lesson5.model.Person;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@ToString
public class BankAccount {

    @Getter
    private final String id;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private double balance;

    @Getter
    private String[] transactionsHistory;

    @Getter
    private final Person owner;

    BankAccount(double balance, Person owner) {
        this.balance = balance;
        this.owner = owner;
        this.transactionsHistory = new String[5];
        this.id = generateUniqueId();
    }

    void addNewTransaction(String newTransaction) {
        for (int i = transactionsHistory.length - 1; i > 0; i--) {
            if (transactionsHistory[i - 1] == null)
                continue;
            transactionsHistory[i] = transactionsHistory[i - 1];
        }
        transactionsHistory[0] = newTransaction;
    }

    private String generateUniqueId() {
        return UUID.randomUUID().toString();
    }

}

