package test.words;

import java.util.Scanner;

public class Game {

    private static Player player1;
    private static Player player2;
    private static String currentWord = "";
    private static String tempWord;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        startGame();
        while (!isStop(currentWord)) {
            do {
                System.out.print(player1.getName() + " your word: ");
                tempWord = scanner.nextLine();
            } while (!checkRule());
            if (isStop(currentWord))
                break;
            do {
                System.out.print(player2.getName() + " your word: ");
                tempWord = scanner.nextLine();
            } while (!checkRule());
        }
    }

    private static boolean isStop(String currentWord) {
        return currentWord.equalsIgnoreCase("stop");
    }

    private static boolean checkRule() {
        if (tempWord == null || tempWord.isEmpty()) {
            System.out.println("The word must not be empty!");
            return false;
        }

        if (currentWord.isEmpty() || isStop(tempWord)) {
            currentWord = tempWord;
            return true;
        }
        if (tempWord.charAt(0) != currentWord.charAt(currentWord.length() - 1)) {
            System.out.println("The word must begin with the last letter of the last word!");
            return false;
        }

        currentWord = tempWord;
        return true;
    }

    private static void startGame() {

        System.out.print("Player 1 input name: ");
        player1 = new Player(scanner.nextLine());
        System.out.print("Player 2 input name: ");
        player2 = new Player(scanner.nextLine());
    }
}
