package test.ticket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HappyTicket {
    public static void main(String[] args) {
        System.out.println(checkTickets(Arrays.asList(
                new Ticket("123321"),
                new Ticket("1322"),
                new Ticket("11"),
                new Ticket("15356"),
                new Ticket("5678"),
                new Ticket("9054"))));
    }

    private static List<Ticket> checkTickets(List<Ticket> tickets) {
        List<Ticket> happyTickets = new ArrayList<>();
        for (Ticket ticket : tickets) {
            if (ticket.getNumber().length() % 2 == 0) {
                if (isEvenNumberHappy(ticket.getNumber())) {
                    happyTickets.add(ticket);
                }
            } else {
                if (isOddNumberHappy(ticket.getNumber())) {
                    happyTickets.add(ticket);
                }
            }
        }
        return happyTickets;
    }

    // if number length is even then ticket num must have left side sum equals right side
    private static boolean isEvenNumberHappy(String number) {
        char[] numerals = number.toCharArray();
        int left = 0;
        int right = 0;
        for (int i = 0; i < numerals.length / 2; i++) {
            left += numerals[i];
            right += numerals[numerals.length - i - 1];
        }
        return left == right;
    }

    // if number length is odd then sum of numerals in odd positions must be equal to sum of numerals in even positions
    private static boolean isOddNumberHappy(String number) {
        char[] numerals = number.toCharArray();
        int evenPos = 0;
        int oddPos = 0;
        for (int i = 0; i < numerals.length; i++) {
            if ((i + 1) % 2 == 0) {
                evenPos += Integer.parseInt(String.valueOf(numerals[i]));
                continue;
            }
            oddPos += Integer.parseInt(String.valueOf(numerals[i]));
        }
        return evenPos == oddPos;
    }
}
