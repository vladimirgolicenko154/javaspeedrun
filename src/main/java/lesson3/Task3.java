package lesson3;

import java.util.Arrays;

/*
Given two arrays of strings a1 and a2 return a sorted array r in lexicographical
order of the strings of a1 which are substrings of strings of a2.

        Example 1:
        a1 = ["arp", "live", "strong"]

        a2 = ["lively", "alive", "harp", "sharp", "armstrong"]

        returns ["arp", "live", "strong"]

        Example 2:
        a1 = ["tarp", "mice", "bull"]

        a2 = ["lively", "alive", "harp", "sharp", "armstrong"]

        returns []

        Notes:
        Arrays are written in "general" notation. See "Your Test Cases" for examples in your language.
        In Shell bash a1 and a2 are strings. The return is a string where words are separated by commas.
        Beware: r must be without duplicates.
        */
public class Task3 {

    public static void main(String[] args) {

        String a[] = new String[]{"arp", "live", "strong"};
        String b[] = new String[]{"lively", "alive", "harp", "sharp", "armstrong"};
        String r[] = new String[]{"arp", "live", "strong"};

        System.out.println(Arrays.toString(inArray(a, b)));

    }


    public static String[] inArray(String[] array1, String[] array2) {
        String[] result = new String[array1.length];
        int i = 0;
        for (String s : array2) {
            for (String s1 : array1) {
                if (s.contains(s1) && !isDuplicate(s1, result))
                    result[i++] = s1;
            }
        }

        Arrays.sort(result);
        return result;
    }

    private static boolean isDuplicate(String s1, String[] arr) {
        for (String s : arr) {
            if (s == null)
                return false;
            if (s.equals(s1))
                return true;
        }
        return false;
    }
}
