package lesson3;

import java.util.Scanner;

/*
 Напишите программу, где пользователь вводит любое целое положительное число.
  программа суммирует все числа от 1 до введенного пользователем числа.
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input the number: ");

        System.out.println("The result is " + sum(scanner.nextInt()));
    }

    private static int sum(int number) {
        if (number < 1)
            throw new IllegalArgumentException("Argument must be positive integer!");
        int result = 0;
        for (int i = 1; i <= number; i++) {
            result += i;
        }
        return result;
    }
}
